package com.anahata.myob.api.transport;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.myob.api.MyobEndPoint;
import com.anahata.myob.api.service.exception.MyobException;
import java.net.HttpURLConnection;
import java.net.URL;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import static com.anahata.myob.api.transport.AbstractMyobTransport.*;
/**
 *
 * @author pablo
 */
@Slf4j
public class HttpURLConnectionMyobTransport extends AbstractMyobTransport{
    
    @Override
    public String sendReceive(MyobEndPoint dataFile, String context, String method, String requestBody, String uID) throws Exception {        
        
        HttpURLConnection conn
                = (HttpURLConnection)new URL(makeURL(dataFile, context, method, uID)).openConnection();        
        
        conn.setRequestProperty(MYOB_API_VERSION_HEADER, getApiVersion());
        
        if (dataFile.getCredentials() != null) {
            conn.setRequestProperty(MYOB_API_CFTOKEN_HEADER, makeCFTokenHeader(dataFile.getCredentials()));
        }
        
        if (dataFile.getOauthAccessToken()!= null) {
            conn.setRequestProperty(MYOB_API_AUTHORIZATION_HEADER, MYOB_API_AUTHORIZATION_HEADER_BEARER + dataFile.getOauthAccessToken().getAccessToken());
            conn.setRequestProperty(MYOB_API_KEY_HEADER, dataFile.getOauthAccessToken().getPlugin().getKey());
        }
        
        if (method != null) {
            conn.setRequestMethod(method);
        }
        
        conn.setRequestProperty("Content-Type", "text/json");
        if (requestBody != null) {
            conn.setDoOutput(true);

            //OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
            IOUtils.write(requestBody, conn.getOutputStream());

            if (conn.getResponseCode() == 201) {
                log.debug("Record is created");
            } else if (conn.getResponseCode() != 200) {
                throw new MyobException(conn.getResponseCode(), conn.getResponseMessage());//IOException(conn.getResponseMessage() + "   " + conn.getResponseCode());
            }

        }

        String ret = IOUtils.toString(conn.getInputStream());

        log.debug("Received {}", ret);

        return ret;

    }

}
