//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.purchase;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.purchase.PurchaseLink;
import com.anahata.myob.api.domain.v2.purchase.SupplierPaymentLineType;

/**
* Describe the SupplierPayment's Line
*/
public class SupplierPaymentLine   
{
    /**
    * Sequence of the entry within the supplier payment set.
    * ONLY required for updating an existing supplier payment line.NOT required when creating a new supplier payment.
    */
    private int RowID;
    public int getRowID() {
        return RowID;
    }

    public void setRowID(int value) {
        RowID = value;
    }

    /**
    * Is it an Bill or an Order
    */
    private SupplierPaymentLineType Type = SupplierPaymentLineType.Purged;
    public SupplierPaymentLineType getType() {
        return Type;
    }

    public void setType(SupplierPaymentLineType value) {
        Type = value;
    }

    /**
    * The Bill or Order Link (only need UID)
    */
    private PurchaseLink Purchase;
    public PurchaseLink getPurchase() {
        return Purchase;
    }

    public void setPurchase(PurchaseLink value) {
        Purchase = value;
    }

    /**
    * Amount applied to the purchase.
    */
    private java.math.BigDecimal AmountApplied;
    public java.math.BigDecimal getAmountApplied() {
        return AmountApplied;
    }

    public void setAmountApplied(java.math.BigDecimal value) {
        AmountApplied = value;
    }

    /**
    * Incrementing number that can be used for change control but does does not preserve a date or a time.
    * ONLY required for updating an existing supplier payment.NOT required when creating a new supplier payment.
    */
    private Long RowVersion;
    public  Long getRowVersion() {
        return RowVersion;
    }

    public void setRowVersion(Long value) {
        RowVersion = value;
    }

}


