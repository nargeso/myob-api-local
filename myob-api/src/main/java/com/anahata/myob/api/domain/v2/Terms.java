//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.TermsPaymentType;

/**
* Base resource for terms definitions
*/
public class Terms   
{
    /**
    * Default Terms of Payment definitions:
    */
    private TermsPaymentType PaymentIsDue = TermsPaymentType.CashOnDelivery;
    public TermsPaymentType getPaymentIsDue() {
        return PaymentIsDue;
    }

    public void setPaymentIsDue(TermsPaymentType value) {
        PaymentIsDue = value;
    }

    /**
    * The discount date which includes EOM (End of Month).
    */
    private int DiscountDate;
    public int getDiscountDate() {
        return DiscountDate;
    }

    public void setDiscountDate(int value) {
        DiscountDate = value;
    }

    /**
    * The balance due date which includes EOM (End of Month).
    */
    private int BalanceDueDate;
    public int getBalanceDueDate() {
        return BalanceDueDate;
    }

    public void setBalanceDueDate(int value) {
        BalanceDueDate = value;
    }

    /**
    * % discount for early payment.
    */
    private java.math.BigDecimal DiscountForEarlyPayment;
    public java.math.BigDecimal getDiscountForEarlyPayment() {
        return DiscountForEarlyPayment;
    }

    public void setDiscountForEarlyPayment(java.math.BigDecimal value) {
        DiscountForEarlyPayment = value;
    }

}


