//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.inventory;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */



/**
* Price levels
*/
public class Levels   
{
    /**
    * Gets or sets the level a price.
    */
    private java.math.BigDecimal LevelA;
    public java.math.BigDecimal getLevelA() {
        return LevelA;
    }

    public void setLevelA(java.math.BigDecimal value) {
        LevelA = value;
    }

    /**
    * Gets or sets the level b price.
    */
    private java.math.BigDecimal LevelB;
    public java.math.BigDecimal getLevelB() {
        return LevelB;
    }

    public void setLevelB(java.math.BigDecimal value) {
        LevelB = value;
    }

    /**
    * Gets or sets the level c price.
    */
    private java.math.BigDecimal LevelC;
    public java.math.BigDecimal getLevelC() {
        return LevelC;
    }

    public void setLevelC(java.math.BigDecimal value) {
        LevelC = value;
    }

    /**
    * Gets or sets the level d price.
    */
    private java.math.BigDecimal LevelD;
    public java.math.BigDecimal getLevelD() {
        return LevelD;
    }

    public void setLevelD(java.math.BigDecimal value) {
        LevelD = value;
    }

    /**
    * Gets or sets the level e price.
    */
    private java.math.BigDecimal LevelE;
    public java.math.BigDecimal getLevelE() {
        return LevelE;
    }

    public void setLevelE(java.math.BigDecimal value) {
        LevelE = value;
    }

    /**
    * Gets or sets the level f price.
    */
    private java.math.BigDecimal LevelF;
    public java.math.BigDecimal getLevelF() {
        return LevelF;
    }

    public void setLevelF(java.math.BigDecimal value) {
        LevelF = value;
    }

}


