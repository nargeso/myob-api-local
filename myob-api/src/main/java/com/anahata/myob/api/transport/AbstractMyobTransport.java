/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.myob.api.transport;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.myob.api.MyobEndPoint;
import com.anahata.myob.api.auth.DataFileCredentials;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import sun.misc.BASE64Encoder;

/**
 *
 * @author pablo
 */
@Slf4j
public abstract class AbstractMyobTransport {

    protected static final String MYOB_API_CFTOKEN_HEADER = "x-myobapi-cftoken";

    protected static final String MYOB_API_VERSION_HEADER = "x-myobapi-version";

    protected static final String MYOB_API_KEY_HEADER = "x-myobapi-key";

    protected static final String MYOB_API_AUTHORIZATION_HEADER = "Authorization";

    protected static final String MYOB_API_AUTHORIZATION_HEADER_BEARER = "Bearer ";

    @Setter
    @Getter
    private String apiVersion = "v2";

    public abstract String sendReceive(MyobEndPoint dataFile, String context, String method, String requestBody, String uID) throws Exception;
    
    protected static String makeCFTokenHeader(DataFileCredentials dataFile) {
        String val = dataFile.getUser() + ":" + dataFile.getPassword();
        return new BASE64Encoder().encode(val.getBytes());
    }
    
    protected static String makeURL(MyobEndPoint dataFile, String context, String method, String uID) {
        if (method == null) {
            method = "GET";
        } else {
            method = method.toUpperCase();
        }

        String url = dataFile.getUri();
        if (context != null) {
            url += "/" + context;
            if (uID != null) {
                url += "/" + uID;
            }
        }

        if (method != null) {
            if (method.equals("PUT") || method.equals("POST")) {
                url += "?returnBody=true";
            }
        }
        
        return url;

    }

    public static void main(String[] args) {
        System.out.println(new BASE64Encoder().encode("Administrator:".getBytes()));
    }
}
