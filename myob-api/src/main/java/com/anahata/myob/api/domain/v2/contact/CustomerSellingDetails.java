//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.contact;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.contact.CustomerCredit;
import com.anahata.myob.api.domain.v2.contact.EmployeeLink;
import com.anahata.myob.api.domain.v2.CustomerTerms;
import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import com.anahata.myob.api.domain.v2.generalledger.TaxCodeLink;
import com.anahata.myob.api.domain.v2.sale.DocumentAction;
import com.anahata.myob.api.domain.v2.sale.InvoiceLayoutType;

/**
* Describes the customer selling details
*/
public class CustomerSellingDetails   
{
    /**
    * The default invoice type for this customer
    */
    private InvoiceLayoutType SaleLayout = InvoiceLayoutType.NoDefault;
    public InvoiceLayoutType getSaleLayout() {
        return SaleLayout;
    }

    public void setSaleLayout(InvoiceLayoutType value) {
        SaleLayout = value;
    }

    /**
    * The default invoice delivery
    */
    private DocumentAction InvoiceDelivery = DocumentAction.Nothing;
    public DocumentAction getInvoiceDelivery() {
        return InvoiceDelivery;
    }

    public void setInvoiceDelivery(DocumentAction value) {
        InvoiceDelivery = value;
    }

    /**
    * The name selected as the default printed form
    */
    private String PrintedForm;
    public String getPrintedForm() {
        return PrintedForm;
    }

    public void setPrintedForm(String value) {
        PrintedForm = value;
    }

    /**
    * The Item price level (read-only)
    */
    private String ItemPriceLevel;
    public String getItemPriceLevel() {
        return ItemPriceLevel;
    }

    public void setItemPriceLevel(String value) {
        ItemPriceLevel = value;
    }

    /**
    * A link to the income account
    */
    private AccountLink IncomeAccount;
    public AccountLink getIncomeAccount() {
        return IncomeAccount;
    }

    public void setIncomeAccount(AccountLink value) {
        IncomeAccount = value;
    }

    /**
    * Default receipt memo
    */
    private String ReceiptMemo;
    public String getReceiptMemo() {
        return ReceiptMemo;
    }

    public void setReceiptMemo(String value) {
        ReceiptMemo = value;
    }

    /**
    * A link to the salesperson resource
    */
    private EmployeeLink SalesPerson;
    public EmployeeLink getSalesPerson() {
        return SalesPerson;
    }

    public void setSalesPerson(EmployeeLink value) {
        SalesPerson = value;
    }

    /**
    * The default sale comment
    */
    private String SaleComment;
    public String getSaleComment() {
        return SaleComment;
    }

    public void setSaleComment(String value) {
        SaleComment = value;
    }

    /**
    * Shipping method text
    */
    private String ShippingMethod;
    public String getShippingMethod() {
        return ShippingMethod;
    }

    public void setShippingMethod(String value) {
        ShippingMethod = value;
    }

    /**
    * The customers houly billing rate
    */
    private java.math.BigDecimal HourlyBillingRate;
    public java.math.BigDecimal getHourlyBillingRate() {
        return HourlyBillingRate;
    }

    public void setHourlyBillingRate(java.math.BigDecimal value) {
        HourlyBillingRate = value;
    }

    /**
    * ABN Number (Must be 11 digits and formatted as XX XXX XXX XXX)
    */
    private String ABN;
    public String getABN() {
        return ABN;
    }

    public void setABN(String value) {
        ABN = value;
    }

    /**
    * The ABN Branch number
    */
    private String ABNBranch;
    public String getABNBranch() {
        return ABNBranch;
    }

    public void setABNBranch(String value) {
        ABNBranch = value;
    }

    /**
    * A link to a tax code resource
    */
    private TaxCodeLink TaxCode;
    public TaxCodeLink getTaxCode() {
        return TaxCode;
    }

    public void setTaxCode(TaxCodeLink value) {
        TaxCode = value;
    }

    /**
    * A link to the tax code resource to apply to freight
    */
    private TaxCodeLink FreightTaxCode;
    public TaxCodeLink getFreightTaxCode() {
        return FreightTaxCode;
    }

    public void setFreightTaxCode(TaxCodeLink value) {
        FreightTaxCode = value;
    }

    /**
    * Indicates whether to use the customer tax code
    */
    private boolean UseCustomerTaxCode;
    public boolean getUseCustomerTaxCode() {
        return UseCustomerTaxCode;
    }

    public void setUseCustomerTaxCode(boolean value) {
        UseCustomerTaxCode = value;
    }

    /**
    * The customer terms
    */
    private CustomerTerms Terms;
    public CustomerTerms getTerms() {
        return Terms;
    }

    public void setTerms(CustomerTerms value) {
        Terms = value;
    }

    /**
    * The customer credit situation
    */
    private CustomerCredit Credit;
    public CustomerCredit getCredit() {
        return Credit;
    }

    public void setCredit(CustomerCredit value) {
        Credit = value;
    }

    /**
    * Tax id number.
    */
    private String TaxIdNumber;
    public String getTaxIdNumber() {
        return TaxIdNumber;
    }

    public void setTaxIdNumber(String value) {
        TaxIdNumber = value;
    }

    /**
    * Default memo text
    */
    private String Memo;
    public String getMemo() {
        return Memo;
    }

    public void setMemo(String value) {
        Memo = value;
    }

}


