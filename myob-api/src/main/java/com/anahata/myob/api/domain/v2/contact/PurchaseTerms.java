//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.contact;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.Terms;
import java.math.BigDecimal;

/**
* Describes additional terms for a purchase
*/
public class PurchaseTerms  extends Terms 
{
    /**
    * The date the discount (if exists) will expire.
    */
    private java.util.Date DiscountExpiryDate = new java.util.Date();
    public java.util.Date getDiscountExpiryDate() {
        return DiscountExpiryDate;
    }

    public void setDiscountExpiryDate(java.util.Date value) {
        DiscountExpiryDate = value;
    }

    /**
    * The discount applicable if the amount if paid before the discount expiry date
    */
    private BigDecimal Discount;
    public BigDecimal getDiscount() {
        return Discount;
    }

    public void setDiscount(BigDecimal value) {
        Discount = value;
    }

    /**
    * Date the balance is due.
    */
    private java.util.Date DueDate = new java.util.Date();
    public java.util.Date getDueDate() {
        return DueDate;
    }

    public void setDueDate(java.util.Date value) {
        DueDate = value;
    }

}


