//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.contact;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.generalledger.JobLink;
import com.anahata.myob.api.domain.v2.payrollCategory.PayrollCategoryLink;
import java.math.BigDecimal;

/**
* Payroll Categories in employee standard pay
*/
public class EmployeeStandardPayCategory   
{
    /**
    * Payroll category
    */
    private PayrollCategoryLink PayrollCategory;
    public PayrollCategoryLink getPayrollCategory() {
        return PayrollCategory;
    }

    public void setPayrollCategory(PayrollCategoryLink value) {
        PayrollCategory = value;
    }

    /**
    * Hours
    */
    private BigDecimal Hours;
    public BigDecimal getHours() {
        return Hours;
    }

    public void setHours(BigDecimal value) {
        Hours = value;
    }

    /**
    * Amount
    */
    private BigDecimal Amount;
    public BigDecimal getAmount() {
        return Amount;
    }

    public void setAmount(BigDecimal value) {
        Amount = value;
    }

    /**
    * Is Hours/Amount calculated or user enter
    */
    private boolean IsCalculated;
    public boolean getIsCalculated() {
        return IsCalculated;
    }

    public void setIsCalculated(boolean value) {
        IsCalculated = value;
    }

    /**
    * Job
    */
    private JobLink Job;
    public JobLink getJob() {
        return Job;
    }

    public void setJob(JobLink value) {
        Job = value;
    }

}


