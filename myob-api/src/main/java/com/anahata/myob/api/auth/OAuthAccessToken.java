/*
 * 
 * To change this license header\nchoose License Headers in Project Properties.
 * To change this template file\nchoose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.myob.api.auth;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;
/**
 *
 * @author pablo
 */
//@Data
@Getter
@Setter
@NoArgsConstructor
@Slf4j
public class OAuthAccessToken implements Serializable {
    //api managed
    private MyobPlugin plugin;     
    private Date createdOn = new Date();    
    //serialized from/to json
    @SerializedName("access_token")
    private String accessToken;
    @SerializedName("refresh_token")
    private String refreshToken;
    @SerializedName("token_type")
    private String tokenType;
    @SerializedName("expires_in")
    private int expiresIn;
    @SerializedName("scope")
    private String scope;

    public OAuthAccessToken(MyobPlugin plugin, String accessToken, String refreshToken, Date createdOn, int expiresIn) {
        this.plugin = plugin;
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.createdOn = createdOn;
        this.expiresIn = expiresIn;
    }
    
    public boolean isExpired(int contingencyMinutes) {
        Date expiryDate = getExpiryDate(contingencyMinutes);
        Date now = new Date();
        boolean ret = createdOn == null || now.after(expiryDate);
        log.debug("isExpired continency = {} min, expiryDate={}, expired=", contingencyMinutes, expiryDate, ret);
        return ret;
    }
    
    public Date getExpiryDate(int contingencyMinutes) {
        if (createdOn == null) {
            return null;
        }
        return DateUtils.addSeconds(createdOn, expiresIn - (contingencyMinutes * 60));
    }
    
    public OAuthAccessToken refresh() throws Exception {
        return OauthAuthenticator.refreshAccessToken(this);
    } 

    @Override
    public String toString() {
        return "OAuthAccessToken{" + "\nplugin=" + plugin + "\ncreatedOn=" + createdOn + "\naccessToken=" + accessToken + "\nrefreshToken=" + refreshToken + "\ntokenType=" + tokenType + "\nexpiresIn=" + expiresIn + "\nscope=" + scope + '}';
    }
    
    
    
}
