//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.generalledger;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.math.BigDecimal;

/**
* Describes an entry in the Journal transaction
*/
public class JournalTransactionLine   
{
    /**
    * A link to the related 
    *  {@link #Account}
    *  resource
    */
    private AccountLink Account;
    public AccountLink getAccount() {
        return Account;
    }

    public void setAccount(AccountLink value) {
        Account = value;
    }

    /**
    * The amount posted
    */
    private BigDecimal Amount;
    public BigDecimal getAmount() {
        return Amount;
    }

    public void setAmount(BigDecimal value) {
        Amount = value;
    }

    /**
    * A link to the related 
    *  {@link #Job}
    *  resource
    */
    private JobLink Job;
    public JobLink getJob() {
        return Job;
    }

    public void setJob(JobLink value) {
        Job = value;
    }

    /**
    * Indicates that the amount posted a credit to the Account object.
    */
    private boolean IsCredit;
    public boolean getIsCredit() {
        return IsCredit;
    }

    public void setIsCredit(boolean value) {
        IsCredit = value;
    }

    /**
    * Line description for each line of the transaction if one has been entered.
    */
    private String LineDescription;
    public String getLineDescription() {
        return LineDescription;
    }

    public void setLineDescription(String value) {
        LineDescription = value;
    }

    /**
    * Date transaction has been reconciled
    */
    private java.util.Date ReconciledDate = new java.util.Date();
    public java.util.Date getReconciledDate() {
        return ReconciledDate;
    }

    public void setReconciledDate(java.util.Date value) {
        ReconciledDate = value;
    }

}


