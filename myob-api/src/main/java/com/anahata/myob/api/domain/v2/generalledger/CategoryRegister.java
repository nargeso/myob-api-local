//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.generalledger;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import com.anahata.myob.api.domain.v2.generalledger.CategoryLink;

/**
* Describes a CategoryRegister resource
*/
public class CategoryRegister   
{
    /**
    * A link to a Category resource
    */
    private CategoryLink Category;
    public CategoryLink getCategory() {
        return Category;
    }

    public void setCategory(CategoryLink value) {
        Category = value;
    }

    /**
    * A link to an Account resource
    */
    private AccountLink Account;
    public AccountLink getAccount() {
        return Account;
    }

    public void setAccount(AccountLink value) {
        Account = value;
    }

    /**
    * Financial year in which the activity was generated ie 2014.
    */
    private int Year;
    public int getYear() {
        return Year;
    }

    public void setYear(int value) {
        Year = value;
    }

    /**
    * Month in which the activity was generated ie December = 12.
    */
    private int Month;
    public int getMonth() {
        return Month;
    }

    public void setMonth(int value) {
        Month = value;
    }

    /**
    * Net activity within profit & loss account or net movement within balance sheet accounts.
    */
    private java.math.BigDecimal Activity;
    public java.math.BigDecimal getActivity() {
        return Activity;
    }

    public void setActivity(java.math.BigDecimal value) {
        Activity = value;
    }

    /**
    * Net activity within profit & loss account or net movement within balance sheet accounts for YearEndAdjustments.
    */
    private java.math.BigDecimal YearEndActivity;
    public java.math.BigDecimal getYearEndActivity() {
        return YearEndActivity;
    }

    public void setYearEndActivity(java.math.BigDecimal value) {
        YearEndActivity = value;
    }

}


