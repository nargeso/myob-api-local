//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.sale;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;

/**
* Describe the response of CustomerPayment/RecordWithDiscountsAndFees resource
*/
public class CustomerPaymentRecordWithDiscountsAndFeesResponse   
{
    /**
    * The link to customerpayment record generated
    */
    private CustomerPaymentLink CustomerPayment;
    public CustomerPaymentLink getCustomerPayment() {
        return CustomerPayment;
    }

    public void setCustomerPayment(CustomerPaymentLink value) {
        CustomerPayment = value;
    }

    /**
    * The link to the finance charge invoice generated due to applied finance charge
    */
    private InvoiceLink FinanceChargeInvoice;
    public InvoiceLink getFinanceChargeInvoice() {
        return FinanceChargeInvoice;
    }

    public void setFinanceChargeInvoice(InvoiceLink value) {
        FinanceChargeInvoice = value;
    }

    /**
    * The negative sale invoices generated due to applied discounts
    */
    private ArrayList<InvoiceLink> DiscountAppliedInvoices = new ArrayList<InvoiceLink>();
    public ArrayList<InvoiceLink> getDiscountAppliedInvoices() {
        return DiscountAppliedInvoices;
    }

    public void setDiscountAppliedInvoices(ArrayList<InvoiceLink> value) {
        DiscountAppliedInvoices = value;
    }

    /**
    * The credit settlements generated due to applied discounts
    */
    private ArrayList<CreditSettlementLink> CreditSettlements = new ArrayList<CreditSettlementLink>();
    public ArrayList<CreditSettlementLink> getCreditSettlements() {
        return CreditSettlements;
    }

    public void setCreditSettlements(ArrayList<CreditSettlementLink> value) {
        CreditSettlements = value;
    }

}


