//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.inventory;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.generalledger.TaxCodeLink;
import java.math.BigDecimal;

/**
* The details when 
*  {@link #Item.IsBought}
*  is true
*/
public class ItemBuyingDetails   
{
    /**
    * The last purchase price (read-only)
    */
    private java.math.BigDecimal LastPurchasePrice;
    public java.math.BigDecimal getLastPurchasePrice() {
        return LastPurchasePrice;
    }

    public void setLastPurchasePrice(java.math.BigDecimal value) {
        LastPurchasePrice = value;
    }

    /**
    * The standard cost
    */
    private java.math.BigDecimal StandardCost;
    public java.math.BigDecimal getStandardCost() {
        return StandardCost;
    }

    public void setStandardCost(java.math.BigDecimal value) {
        StandardCost = value;
    }

    /**
    * The unit of measure when buying items
    */
    private String BuyingUnitOfMeasure;
    public String getBuyingUnitOfMeasure() {
        return BuyingUnitOfMeasure;
    }

    public void setBuyingUnitOfMeasure(String value) {
        BuyingUnitOfMeasure = value;
    }

    /**
    * The number of items in a buying unit
    */
    private BigDecimal ItemsPerBuyingUnit;
    public BigDecimal getItemsPerBuyingUnit() {
        return ItemsPerBuyingUnit;
    }

    public void setItemsPerBuyingUnit(BigDecimal value) {
        ItemsPerBuyingUnit = value;
    }

    /**
    * The tax code
    */
    private TaxCodeLink TaxCode;
    public TaxCodeLink getTaxCode() {
        return TaxCode;
    }

    public void setTaxCode(TaxCodeLink value) {
        TaxCode = value;
    }

    /**
    * The restocking information
    */
    private RestockingInformation RestockingInformation;
    public RestockingInformation getRestockingInformation() {
        return RestockingInformation;
    }

    public void setRestockingInformation(RestockingInformation value) {
        RestockingInformation = value;
    }

}


