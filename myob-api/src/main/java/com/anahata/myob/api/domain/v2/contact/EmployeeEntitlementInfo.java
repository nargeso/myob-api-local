//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.contact;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.payrollCategory.PayrollCategoryLink;

/**
* Employment Entitlement Info
*/
public class EmployeeEntitlementInfo   
{
    /**
    * Entitlement Category
    */
    private PayrollCategoryLink EntitlementCategory;
    public PayrollCategoryLink getEntitlementCategory() {
        return EntitlementCategory;
    }

    public void setEntitlementCategory(PayrollCategoryLink value) {
        EntitlementCategory = value;
    }

    /**
    * Current entitlement is assigned
    */
    private boolean IsAssigned;
    public boolean getIsAssigned() {
        return IsAssigned;
    }

    public void setIsAssigned(boolean value) {
        IsAssigned = value;
    }

    /**
    * Carry Over
    */
    private java.math.BigDecimal CarryOver;
    public java.math.BigDecimal getCarryOver() {
        return CarryOver;
    }

    public void setCarryOver(java.math.BigDecimal value) {
        CarryOver = value;
    }

    /**
    * Year To Date
    */
    private java.math.BigDecimal YearToDate;
    public java.math.BigDecimal getYearToDate() {
        return YearToDate;
    }

    public void setYearToDate(java.math.BigDecimal value) {
        YearToDate = value;
    }

    /**
    * Total
    */
    private java.math.BigDecimal Total;
    public java.math.BigDecimal getTotal() {
        return Total;
    }

    public void setTotal(java.math.BigDecimal value) {
        Total = value;
    }

}


