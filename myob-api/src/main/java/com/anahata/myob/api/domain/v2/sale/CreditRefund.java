//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.sale;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.contact.CustomerLink;
import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import com.anahata.myob.api.domain.v2.sale.DocumentAction;
import com.anahata.myob.api.domain.v2.sale.InvoiceLink;

/**
* Describe the Sale/CreditRefund resource
*/
public class CreditRefund  extends BaseEntity 
{
    /**
    * The account
    */
    private AccountLink Account;
    public AccountLink getAccount() {
        return Account;
    }

    public void setAccount(AccountLink value) {
        Account = value;
    }

    /**
    * The invoice to which the refund originated
    */
    private InvoiceLink Invoice;
    public InvoiceLink getInvoice() {
        return Invoice;
    }

    public void setInvoice(InvoiceLink value) {
        Invoice = value;
    }

    /**
    * The customer to whom the refund was made
    */
    private CustomerLink Customer;
    public CustomerLink getCustomer() {
        return Customer;
    }

    public void setCustomer(CustomerLink value) {
        Customer = value;
    }

    /**
    * The address of the Payee
    */
    private String Payee;
    public String getPayee() {
        return Payee;
    }

    public void setPayee(String value) {
        Payee = value;
    }

    /**
    * The cheue number
    */
    private String Number;
    public String getNumber() {
        return Number;
    }

    public void setNumber(String value) {
        Number = value;
    }

    /**
    * The data the refund was made
    */
    private java.util.Date Date = new java.util.Date();
    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date value) {
        Date = value;
    }

    /**
    * The amount of the refund
    */
    private java.math.BigDecimal Amount;
    public java.math.BigDecimal getAmount() {
        return Amount;
    }

    public void setAmount(java.math.BigDecimal value) {
        Amount = value;
    }

    /**
    * additional information
    */
    private String Memo;
    public String getMemo() {
        return Memo;
    }

    public void setMemo(String value) {
        Memo = value;
    }

    /**
    * Was a cheque printed
    */
    private boolean ChequePrinted;
    public boolean getChequePrinted() {
        return ChequePrinted;
    }

    public void setChequePrinted(boolean value) {
        ChequePrinted = value;
    }

    /**
    * The current delivery status of the refund
    */
    private DocumentAction DeliveryStatus = DocumentAction.Nothing;
    public DocumentAction getDeliveryStatus() {
        return DeliveryStatus;
    }

    public void setDeliveryStatus(DocumentAction value) {
        DeliveryStatus = value;
    }

}


