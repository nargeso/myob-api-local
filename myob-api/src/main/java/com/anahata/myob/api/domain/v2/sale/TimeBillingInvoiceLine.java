//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.sale;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.inventory.ItemLink;
import com.anahata.myob.api.domain.v2.payrollCategory.ActivityLink;
import java.math.BigDecimal;

/**
* Describe the Sale/Invoice/TimeBilling's Lines
*/
public class TimeBillingInvoiceLine  extends InvoiceLine 
{
    /**
    * The date the service/item was provided
    */
    private java.util.Date Date = new java.util.Date();
    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date value) {
        Date = value;
    }

    /**
    * The hours spent doing the 
    *  {@link #TimeBillingInvoiceLine.Activity}
    */
    private BigDecimal Hours;
    public BigDecimal getHours() {
        return Hours;
    }

    public void setHours(BigDecimal value) {
        Hours = value;
    }

    /**
    * The 
    *  {@link #MYOB.AccountRight.SDK.Contracts.Version2.TimeBilling.Activity}
    *  being performed
    */
    private ActivityLink Activity;
    public ActivityLink getActivity() {
        return Activity;
    }

    public void setActivity(ActivityLink value) {
        Activity = value;
    }

    /**
    * The number of Units of the 
    *  {@link #TimeBillingInvoiceLine.Item}
    */
    private BigDecimal Units;
    public BigDecimal getUnits() {
        return Units;
    }

    public void setUnits(BigDecimal value) {
        Units = value;
    }

    /**
    * The 
    *  {@link #MYOB.AccountRight.SDK.Contracts.Version2.Inventory.Item}
    *  being supplied
    */
    private ItemLink Item;
    public ItemLink getItem() {
        return Item;
    }

    public void setItem(ItemLink value) {
        Item = value;
    }

    /**
    * The rate (if activity) or cost (if item) per hour or unit
    */
    private BigDecimal Rate;
    public BigDecimal getRate() {
        return Rate;
    }

    public void setRate(BigDecimal value) {
        Rate = value;
    }

}


