//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.ProductLevel;

/**
* A resource description describing the path, version and applicable company file versions supported
*/
public class VersionMap   
{
    /**
    * The resource path to use after the companyfile ID
    */
    private String ResourcePath;
    public String getResourcePath() {
        return ResourcePath;
    }

    public void setResourcePath(String value) {
        ResourcePath = value;
    }

    /**
    * The version this resource is applicable to
    */
    private String Version;
    public String getVersion() {
        return Version;
    }

    public void setVersion(String value) {
        Version = value;
    }

    /**
    * The company file version this resource is supported from
    */
    private String FromProductVersion;
    public String getFromProductVersion() {
        return FromProductVersion;
    }

    public void setFromProductVersion(String value) {
        FromProductVersion = value;
    }

    /**
    * The company file version this resource is supported to
    */
    private String ToProductVersion;
    public String getToProductVersion() {
        return ToProductVersion;
    }

    public void setToProductVersion(String value) {
        ToProductVersion = value;
    }

    /**
    * The comapny file version this resource for Mininum Product Level
    */
    private ProductLevel MinimumProductLevel;
    public ProductLevel getMinimumProductLevel() {
        return MinimumProductLevel;
    }

    public void setMinimumProductLevel(ProductLevel value) {
        MinimumProductLevel = value;
    }

}


