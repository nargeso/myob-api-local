//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:20 PM
//

package com.anahata.myob.api.domain;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.ProductLevel;

/**
* CompanyFile
*/
public class CompanyFile   
{
    /**
    * The CompanyFile Identifier
    */
    private String Id;
    public String getId() {
        return Id;
    }

    public void setId(String value) {
        Id = value;
    }

    /**
    * CompanyFile Name
    */
    private String Name;
    public String getName() {
        return Name;
    }

    public void setName(String value) {
        Name = value;
    }

    /**
    * CompanyFile Library full path (including file name).
    */
    private String LibraryPath;
    public String getLibraryPath() {
        return LibraryPath;
    }

    public void setLibraryPath(String value) {
        LibraryPath = value;
    }

    /**
    * Account Right Live product version compatible with this CompanyFile
    */
    private String ProductVersion;
    public String getProductVersion() {
        return ProductVersion;
    }

    public void setProductVersion(String value) {
        ProductVersion = value;
    }

    /**
    * Account Right Live product level of the CompanyFile
    */
    private ProductLevel ProductLevel;
    public ProductLevel getProductLevel() {
        return ProductLevel;
    }

    public void setProductLevel(ProductLevel value) {
        ProductLevel = value;
    }

    /**
    * String of the resource
    */
    private String URI;
    public String getURI() {
        return URI;
    }

    public void setString(String value) {
        URI = value;
    }

}


