//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.generalledger;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import com.anahata.myob.api.domain.v2.generalledger.JobLink;
import com.anahata.myob.api.domain.v2.generalledger.TaxCodeLink;

/**
* GeneralJournalLine schema
*/
public class GeneralJournalLine   
{
    /**
    * Row's Id (Read Only)
    */
    private int RowId;
    public int getRowId() {
        return RowId;
    }

    public void setRowId(int value) {
        RowId = value;
    }

    /**
    * Row's timestamp (Read Only)
    */
    private Long RowVersion;
    public  Long getRowVersion() {
        return RowVersion;
    }

    public void setRowVersion(Long value) {
        RowVersion = value;
    }

    /**
    * Account Number
    */
    private AccountLink Account;
    public AccountLink getAccount() {
        return Account;
    }

    public void setAccount(AccountLink value) {
        Account = value;
    }

    /**
    * Job Id
    */
    private JobLink Job;
    public JobLink getJob() {
        return Job;
    }

    public void setJob(JobLink value) {
        Job = value;
    }

    /**
    * Memo
    */
    private String Memo;
    public String getMemo() {
        return Memo;
    }

    public void setMemo(String value) {
        Memo = value;
    }

    /**
    * Tax Code
    */
    private TaxCodeLink TaxCode;
    public TaxCodeLink getTaxCode() {
        return TaxCode;
    }

    public void setTaxCode(TaxCodeLink value) {
        TaxCode = value;
    }

    /**
    * Represents a Credit Amount if 
    *  {@link #IsCredit}
    *  equals True
    * Represents a Debit Amount if 
    *  {@link #IsCredit}
    *  equals False
    */
    private java.math.BigDecimal Amount;
    public java.math.BigDecimal getAmount() {
        return Amount;
    }

    public void setAmount(java.math.BigDecimal value) {
        Amount = value;
    }

    /**
    * {@link #Amount}
    *  Is Credit
    */
    private boolean IsCredit;
    public boolean getIsCredit() {
        return IsCredit;
    }

    public void setIsCredit(boolean value) {
        IsCredit = value;
    }

    /**
    * Tax Amount
    * The value written in Tax Amount is only taken into account if 
    *  {@link #IsOverriddenTaxAmount}
    *  equals True
    */
    private java.math.BigDecimal TaxAmount;
    public java.math.BigDecimal getTaxAmount() {
        return TaxAmount;
    }

    public void setTaxAmount(java.math.BigDecimal value) {
        TaxAmount = value;
    }

    /**
    * By setting this flag to True, the Tax will be set to the value of TaxAmount.
    */
    private boolean IsOverriddenTaxAmount;
    public boolean getIsOverriddenTaxAmount() {
        return IsOverriddenTaxAmount;
    }

    public void setIsOverriddenTaxAmount(boolean value) {
        IsOverriddenTaxAmount = value;
    }

}


