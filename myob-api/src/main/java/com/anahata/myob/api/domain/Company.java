//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:20 PM
//

package com.anahata.myob.api.domain;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */



/**
* Company's info
*/
public class Company   
{
    /**
    * Company Name
    */
    private String CompanyName;
    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String value) {
        CompanyName = value;
    }

    /**
    * Serial Number
    */
    private String SerialNumber;
    public String getSerialNumber() {
        return SerialNumber;
    }

    public void setSerialNumber(String value) {
        SerialNumber = value;
    }

    /**
    * Company Address
    */
    private String Address;
    public String getAddress() {
        return Address;
    }

    public void setAddress(String value) {
        Address = value;
    }

    /**
    * Company Phone Number
    */
    private String PhoneNumber;
    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String value) {
        PhoneNumber = value;
    }

    /**
    * Company Fax Number
    */
    private String FaxNumber;
    public String getFaxNumber() {
        return FaxNumber;
    }

    public void setFaxNumber(String value) {
        FaxNumber = value;
    }

    /**
    * Company Email Address
    */
    private String EmailAddress;
    public String getEmailAddress() {
        return EmailAddress;
    }

    public void setEmailAddress(String value) {
        EmailAddress = value;
    }

    /**
    * Australian Business Number (ABN) OR  Tax File Number (TFN)
    * ONLY for AU Company File
    */
    private String ABNOrTFN;
    public String getABNOrTFN() {
        return ABNOrTFN;
    }

    public void setABNOrTFN(String value) {
        ABNOrTFN = value;
    }

    /**
    * ABN Branch
    * ONLY for AU Company File
    */
    private String ABNBranch;
    public String getABNBranch() {
        return ABNBranch;
    }

    public void setABNBranch(String value) {
        ABNBranch = value;
    }

    /**
    * Australian Company Number
    * ONLY for AU Company File
    */
    private String ACN;
    public String getACN() {
        return ACN;
    }

    public void setACN(String value) {
        ACN = value;
    }

    /**
    * Sales Tax Number
    * ONLY for AU Company File
    */
    private String SalesTaxNumber;
    public String getSalesTaxNumber() {
        return SalesTaxNumber;
    }

    public void setSalesTaxNumber(String value) {
        SalesTaxNumber = value;
    }

    /**
    * Payee Number
    * ONLY for AU Company File
    */
    private String PayeeNumber;
    public String getPayeeNumber() {
        return PayeeNumber;
    }

    public void setPayeeNumber(String value) {
        PayeeNumber = value;
    }

    /**
    * GST Registration Number
    * ONLY for NZ Company File
    */
    private String GSTRegistrationNumber;
    public String getGSTRegistrationNumber() {
        return GSTRegistrationNumber;
    }

    public void setGSTRegistrationNumber(String value) {
        GSTRegistrationNumber = value;
    }

    /**
    * Company Registration Number
    * ONLY for NZ Company File
    */
    private String CompanyRegistrationNumber;
    public String getCompanyRegistrationNumber() {
        return CompanyRegistrationNumber;
    }

    public void setCompanyRegistrationNumber(String value) {
        CompanyRegistrationNumber = value;
    }

    /**
    * True indicates the company file is readonly.False indicates the company file not readonly.
    */
    private boolean ReadOnly;
    public boolean getReadOnly() {
        return ReadOnly;
    }

    public void setReadOnly(boolean value) {
        ReadOnly = value;
    }

}


