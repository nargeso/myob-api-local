//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.sale;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.sale.CustomerPaymentLineType;

/**
* Describe the Sale/CustomerPayment's resource line
*/
public class CustomerPaymentLine   
{
    /**
    * Sequence of the entry within the customer payment set.
    */
    private int RowID;
    public int getRowID() {
        return RowID;
    }

    public void setRowID(int value) {
        RowID = value;
    }

    /**
    * Sales invoice number
    */
    private String Number;
    public String getNumber() {
        return Number;
    }

    public void setNumber(String value) {
        Number = value;
    }

    /**
    * Unique guid identifier belonging to the invoice assigned to the customer payment.
    */
    private String UID;
    public String getUID() {
        return UID;
    }

    public void setUID(String value) {
        UID = value;
    }

    /**
    * Amount applied to invoice.
    */
    private java.math.BigDecimal AmountApplied;
    public java.math.BigDecimal getAmountApplied() {
        return AmountApplied;
    }

    public void setAmountApplied(java.math.BigDecimal value) {
        AmountApplied = value;
    }

    /**
    * The customer payment line type
    */
    private CustomerPaymentLineType Type = CustomerPaymentLineType.Invoice;
    public CustomerPaymentLineType getType() {
        return Type;
    }

    public void setType(CustomerPaymentLineType value) {
        Type = value;
    }

    /**
    * Uniform resource identifier associated with the invoice object.
    */
    private String URI;
    public String getURI() {
        return URI;
    }

    public void setString(String value) {
        URI = value;
    }

}


