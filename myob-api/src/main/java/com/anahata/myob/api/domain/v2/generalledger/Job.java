//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.generalledger;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.contact.CardLink;

/**
* Describes a Job resource
*/
public class Job  extends BaseEntity 
{
    /**
    * Number assigned to the job.
    */
    private String Number;
    public String getNumber() {
        return Number;
    }

    public void setNumber(String value) {
        Number = value;
    }

    /**
    * Indicates the job is a header.
    */
    private boolean IsHeader;
    public boolean getIsHeader() {
        return IsHeader;
    }

    public void setIsHeader(boolean value) {
        IsHeader = value;
    }

    /**
    * Name assigned to the job.
    */
    private String Name;
    public String getName() {
        return Name;
    }

    public void setName(String value) {
        Name = value;
    }

    /**
    * Description text for the job.
    */
    private String Description;
    public String getDescription() {
        return Description;
    }

    public void setDescription(String value) {
        Description = value;
    }

    /**
    * A link to the parent 
    *  {@link #Job}
    *  resource
    */
    private JobLink ParentJob;
    public JobLink getParentJob() {
        return ParentJob;
    }

    public void setParentJob(JobLink value) {
        ParentJob = value;
    }

    /**
    * A link to a related customer resource
    */
    private CardLink LinkedCustomer;
    public CardLink getLinkedCustomer() {
        return LinkedCustomer;
    }

    public void setLinkedCustomer(CardLink value) {
        LinkedCustomer = value;
    }

    /**
    * % of the job completed.
    */
    private java.math.BigDecimal PercentComplete;
    public java.math.BigDecimal getPercentComplete() {
        return PercentComplete;
    }

    public void setPercentComplete(java.math.BigDecimal value) {
        PercentComplete = value;
    }

    /**
    * The job start date
    */
    private java.util.Date StartDate = new java.util.Date();
    public java.util.Date getStartDate() {
        return StartDate;
    }

    public void setStartDate(java.util.Date value) {
        StartDate = value;
    }

    /**
    * The job completion date
    */
    private java.util.Date FinishDate = new java.util.Date();
    public java.util.Date getFinishDate() {
        return FinishDate;
    }

    public void setFinishDate(java.util.Date value) {
        FinishDate = value;
    }

    /**
    * The job contact
    */
    private String Contact;
    public String getContact() {
        return Contact;
    }

    public void setContact(String value) {
        Contact = value;
    }

    /**
    * The job manager
    */
    private String Manager;
    public String getManager() {
        return Manager;
    }

    public void setManager(String value) {
        Manager = value;
    }

    /**
    * Indicates the job is active.
    */
    private boolean IsActive;
    public boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(boolean value) {
        IsActive = value;
    }

    /**
    * Indicates if a job is used to track reimbursable expenses.
    */
    private boolean TrackReimbursables;
    public boolean getTrackReimbursables() {
        return TrackReimbursables;
    }

    public void setTrackReimbursables(boolean value) {
        TrackReimbursables = value;
    }

}


