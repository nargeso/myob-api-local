//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:20 PM
//

package com.anahata.myob.api.domain.v2.company;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.generalledger.TaxCodeLink;
import com.anahata.myob.api.domain.v2.Terms;

/**
* Purchases' terms preferences
*/
public class CompanyPurchasesPreferencesTerms  extends Terms 
{
    /**
    * Tax code of purchases
    */
    private TaxCodeLink TaxCode;
    public TaxCodeLink getTaxCode() {
        return TaxCode;
    }

    public void setTaxCode(TaxCodeLink value) {
        TaxCode = value;
    }

    /**
    * Use supplier's tax code instead of default tax code
    */
    private boolean UseSupplierTaxCode;
    public boolean getUseSupplierTaxCode() {
        return UseSupplierTaxCode;
    }

    public void setUseSupplierTaxCode(boolean value) {
        UseSupplierTaxCode = value;
    }

    /**
    * Tax code of purchase freight
    */
    private TaxCodeLink FreightTaxCode;
    public TaxCodeLink getFreightTaxCode() {
        return FreightTaxCode;
    }

    public void setFreightTaxCode(TaxCodeLink value) {
        FreightTaxCode = value;
    }

    /**
    * Credit limit of a purchase
    */
    private java.math.BigDecimal CreditLimit;
    public java.math.BigDecimal getCreditLimit() {
        return CreditLimit;
    }

    public void setCreditLimit(java.math.BigDecimal value) {
        CreditLimit = value;
    }

}


