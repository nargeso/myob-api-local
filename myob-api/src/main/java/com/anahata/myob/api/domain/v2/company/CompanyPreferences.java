//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:20 PM
//

package com.anahata.myob.api.domain.v2.company;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.company.CompanyBankingPreferences;
import com.anahata.myob.api.domain.v2.company.CompanyPurchasesPreferences;
import com.anahata.myob.api.domain.v2.company.CompanyReportsAndFormsPreferences;
import com.anahata.myob.api.domain.v2.company.CompanySalesPreferences;
import com.anahata.myob.api.domain.v2.company.CompanySystemPreferences;
import com.anahata.myob.api.domain.v2.company.CompanyTimesheetPreferences;

/**
* Company preferences
*/
public class CompanyPreferences   
{
    /**
    * System preferences
    */
    private CompanySystemPreferences System;
    public CompanySystemPreferences getSystem() {
        return System;
    }

    public void setSystem(CompanySystemPreferences value) {
        System = value;
    }

    /**
    * Reports and forms preferences
    */
    private CompanyReportsAndFormsPreferences ReportsAndForms;
    public CompanyReportsAndFormsPreferences getReportsAndForms() {
        return ReportsAndForms;
    }

    public void setReportsAndForms(CompanyReportsAndFormsPreferences value) {
        ReportsAndForms = value;
    }

    /**
    * Banking preferences
    */
    private CompanyBankingPreferences Banking;
    public CompanyBankingPreferences getBanking() {
        return Banking;
    }

    public void setBanking(CompanyBankingPreferences value) {
        Banking = value;
    }

    /**
    * Sales preferences
    */
    private CompanySalesPreferences Sales;
    public CompanySalesPreferences getSales() {
        return Sales;
    }

    public void setSales(CompanySalesPreferences value) {
        Sales = value;
    }

    /**
    * Purchase preferences
    */
    private CompanyPurchasesPreferences Purchases;
    public CompanyPurchasesPreferences getPurchases() {
        return Purchases;
    }

    public void setPurchases(CompanyPurchasesPreferences value) {
        Purchases = value;
    }

    /**
    * Timesheet preferences
    */
    private CompanyTimesheetPreferences Timesheets;
    public CompanyTimesheetPreferences getTimesheets() {
        return Timesheets;
    }

    public void setTimesheets(CompanyTimesheetPreferences value) {
        Timesheets = value;
    }

    /**
    * A number that can be used for change control but does not preserve a date or a time.
    */
    private String RowVersion;
    public String getRowVersion() {
        return RowVersion;
    }

    public void setRowVersion(String value) {
        RowVersion = value;
    }

}


