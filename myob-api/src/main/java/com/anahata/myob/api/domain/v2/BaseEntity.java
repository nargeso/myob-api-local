//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:20 PM
//

package com.anahata.myob.api.domain.v2;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */



/**
* Commonly shared properties for main entities
*/
public abstract class BaseEntity   
{
    /**
    * Unique account identifier in the form of a guid.
    * 
    * ONLY required for updating an existing account. 
    * NOT required when creating a new account.
    */
    private String UID;
    public String getUID() {
        return UID;
    }

    public void setUID(String value) {
        UID = value;
    }

    /**
    * The location where this entity can be retrieved. (Read only)
    */
    private String URI;
    public String getURI() {
        return URI;
    }

    public void setString(String value) {
        URI = value;
    }

    /**
    * A number that can be used for change control but does not preserve a date or a time. 
    * ONLY required for updating an existing account. 
    * NOT required when creating a new account.
    */
    private String RowVersion;
    public String getRowVersion() {
        return RowVersion;
    }

    public void setRowVersion(String value) {
        RowVersion = value;
    }

    /**
    * If supported then the time the entity was created, or a default value if the database was upgraded and the time of creating was unknown and cannot be determined. (Read only)
    */
    private java.util.Date Created = new java.util.Date();
    public java.util.Date getCreated() {
        return Created;
    }

    public void setCreated(java.util.Date value) {
        Created = value;
    }

    /**
    * If supported then the time the entity was last modified, or a default value if the database was upgraded and the time of last modification was unknown and cannot be determined. (Read only)
    */
    private java.util.Date LastModified = new java.util.Date();
    public java.util.Date getLastModified() {
        return LastModified;
    }

    public void setLastModified(java.util.Date value) {
        LastModified = value;
    }

}


