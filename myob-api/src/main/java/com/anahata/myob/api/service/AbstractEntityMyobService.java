/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.myob.api.service;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.EntityPage;
import com.anahata.myob.api.service.exception.MyobException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author pablo
 * @param <T>
 * @param <U>
 */
@Slf4j
public abstract class AbstractEntityMyobService<T extends EntityPage<U>, U extends BaseEntity> extends AbstractMyobService{

    private final Class<T> pageType;

    private final Class<U> entityType;

    protected AbstractEntityMyobService(String context, Class<T> pageType, Class<U> entityType) {
        super(context);
        this.pageType = pageType;
        this.entityType = entityType;
    }
    
    public List<U> findAll() throws MyobException {
        String ret = sendReceive(null, null, null);        
        T all = fromJson(ret, pageType);
        return all.getItems();
    }

    public U find(String uid) throws MyobException {
        String ret = sendReceive(null, null, uid);        
        U all = fromJson(ret, entityType);
        return all;
    }
    
    public U update(U item) throws MyobException {        
        String ret = sendReceive("PUT", toJson(item), item.getUID());        
        item = fromJson(ret, entityType);
        return item;
    }

    public U create(U item) throws MyobException {        
        String ret = sendReceive("POST", toJson(item), null);
        return fromJson(ret, entityType);
    }

}
