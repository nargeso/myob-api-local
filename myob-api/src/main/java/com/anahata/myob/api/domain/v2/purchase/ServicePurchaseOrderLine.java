//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.purchase;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import com.anahata.myob.api.domain.v2.generalledger.JobLink;
import com.anahata.myob.api.domain.v2.generalledger.TaxCodeLink;
import com.anahata.myob.api.domain.v2.sale.OrderLine;

/**
* Describes a line for a 
*  {@link #ServicePurchaseOrder}
*/
public class ServicePurchaseOrderLine  extends OrderLine 
{
    /**
    * The line amount (depends on the value of 
    *  {@link #PurchaseOrder.IsTaxInclusive}
    *  in the parent 
    *  {@link #PurchaseOrder}
    * )
    * 
    * Only applicable when 
    *  {@link #OrderLine.Type}
    * =
    *  {@link #OrderLineType.Transaction}
    */
    private java.math.BigDecimal Total;
    public java.math.BigDecimal getTotal() {
        return Total;
    }

    public void setTotal(java.math.BigDecimal value) {
        Total = value;
    }

    /**
    * The account
    * 
    * Only applicable when 
    *  {@link #OrderLine.Type}
    * =
    *  {@link #OrderLineType.Transaction}
    */
    private AccountLink Account;
    public AccountLink getAccount() {
        return Account;
    }

    public void setAccount(AccountLink value) {
        Account = value;
    }

    /**
    * The job
    * 
    * Only applicable when 
    *  {@link #OrderLine.Type}
    * =
    *  {@link #OrderLineType.Transaction}
    */
    private JobLink Job;
    public JobLink getJob() {
        return Job;
    }

    public void setJob(JobLink value) {
        Job = value;
    }

    /**
    * The applied tax code
    * 
    * Only applicable when 
    *  {@link #OrderLine.Type}
    * =
    *  {@link #OrderLineType.Transaction}
    */
    private TaxCodeLink TaxCode;
    public TaxCodeLink getTaxCode() {
        return TaxCode;
    }

    public void setTaxCode(TaxCodeLink value) {
        TaxCode = value;
    }

}


