//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */



/**
* Describes an identifier
*/
public class Identifier   
{
    /**
    * The label assigned to identify the value
    * 
    * This entry is read only and can be manipulated using the AccountRight application
    */
    private String Label;
    public String getLabel() {
        return Label;
    }

    public void setLabel(String value) {
        Label = value;
    }

    /**
    * The data to store
    */
    private String Value;
    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

}


