/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.myob.api.auth;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.myob.api.MyobEndPoint;
import com.anahata.myob.api.MyobEndPointProvider;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.w3c.dom.Document;
import static com.anahata.myob.api.auth.OauthAuthenticator.*;
import static java.net.URLDecoder.*;

/**
 *
 * @author pablo
 */
@Slf4j
public class JavaFXOAuthAuthenticatorApp extends Application {
    private static final String CODE_PARAMETER = "code=";

    private static MyobPlugin plugin;

    private static OauthLoginCallBack callBack;

    public static OAuthAccessToken ret;

    private static Stage mainStage;

    public static void loginAndGetToken(MyobPlugin plugin) throws Exception {
        JavaFXOAuthAuthenticatorApp.plugin = plugin;
        JavaFXOAuthAuthenticatorApp.callBack = new OauthLoginCallBack() {
            @Override
            public void userLogon(String code) {
                try {
                    ret = OauthAuthenticator.getAccessToken(plugin, code);
                } catch (Exception e) {
                    log.error("Exception getting access token", e);
                    throw new RuntimeException(e);
                }
                Platform.exit();
                //mainStage.close();
            }
        };
        launch();

    }

    @Override
    public void start(Stage stage) throws Exception {
        mainStage = stage;
        login(stage, plugin, callBack);
    }

    /**
     * Login using JavaFX WebView.
     *
     * @param stage    the stage
     * @param callBack the callBack
     * @throws Exception
     */
    public static void login(@NonNull Stage stage, @NonNull MyobPlugin plugin, @NonNull OauthLoginCallBack callBack) throws Exception {

        WebView wb = new WebView();
        stage.setWidth(500);
        stage.setHeight(500);
        stage.setScene(new Scene(wb));
        stage.show();
        String url = getloginUrl(plugin.getKey(), plugin.getRedirectURL());
        log.debug("Opening {}", url);        
        wb.getEngine().documentProperty().addListener(new ChangeListener<Document>() {
            @Override
            public void changed(
                    ObservableValue<? extends Document> observable, Document oldValue, Document newValue) {
                //System.out.println("wb.getEngine().getLocation() = " + wb.getEngine().getLocation());
                String loc = wb.getEngine().getLocation();
                if (loc.contains(CODE_PARAMETER)) {
                    //System.out.println("wb.getEngine().getTitle()=" + wb.getEngine().getTitle());
                    final String code = decode(loc.substring(loc.indexOf(CODE_PARAMETER) + CODE_PARAMETER.length()));
                    callBack.userLogon(code);
                    stage.close();
                }
            }
        });
        wb.getEngine().load(url);

    }

    public static void loginAndGetToken(MyobEndPointProvider provider) throws Exception {
        Stage stage = new Stage();
        final MyobEndPoint mep = provider.getEndPoint();
        login(stage, mep.getOauthAccessToken().getPlugin(), (String code) -> {
            try {
                OAuthAccessToken token = OauthAuthenticator.getAccessToken(mep.getOauthAccessToken().getPlugin(), code);
                mep.setOauthAccessToken(token);
                provider.oauthTokenRefreshed(mep);
            } catch (Exception e) {
                log.error("Exception getting access token", e);
            }
        });

    }
}
