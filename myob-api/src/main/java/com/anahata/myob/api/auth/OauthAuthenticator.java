/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.myob.api.auth;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.gson.Gson;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

/**
 *
 * @author pablo
 */
@Slf4j
public class OauthAuthenticator {
    private static final String MYOB_OAUTH2_TOKEN_URL = "https://secure.myob.com/oauth2/v1/authorize";
    private static final String MYOB_OAUTH2_ACCOUNTLOGIN_URL = "https://secure.myob.com/oauth2/account/authorize";
    

    private static final org.apache.http.client.HttpClient httpClient = HttpClientBuilder.create().build();

    public static String getloginUrl(String key, String redirectURL) {
        return MYOB_OAUTH2_ACCOUNTLOGIN_URL + "?client_id=" + key + "&redirect_uri=" + encode(
                redirectURL) + "&response_type=code&scope=CompanyFile";
    }

    public static OAuthAccessToken refreshAccessToken(OAuthAccessToken currentToken) throws Exception {
        List<NameValuePair> arguments = new ArrayList<>();
        arguments.add(new BasicNameValuePair("client_id", currentToken.getPlugin().getKey()));
        arguments.add(new BasicNameValuePair("client_secret", currentToken.getPlugin().getSecret()));
        arguments.add(new BasicNameValuePair("refresh_token", currentToken.getRefreshToken()));
        arguments.add(new BasicNameValuePair("grant_type", "refresh_token"));
        OAuthAccessToken ret = authorize(arguments);
        ret.setPlugin(currentToken.getPlugin());
        return ret;

    }

    public static OAuthAccessToken getAccessToken(@NonNull MyobPlugin plugin, String code) throws Exception {
        
        List<NameValuePair> arguments = new ArrayList<>();
        arguments.add(new BasicNameValuePair("client_id", plugin.getKey()));
        arguments.add(new BasicNameValuePair("client_secret", plugin.getSecret()));
        arguments.add(new BasicNameValuePair("code", code));
        arguments.add(new BasicNameValuePair("redirect_uri", plugin.getRedirectURL()));
        arguments.add(new BasicNameValuePair("scope", "CompanyFile"));
        arguments.add(new BasicNameValuePair("grant_type", "authorization_code"));
        OAuthAccessToken ret = authorize(arguments);
        ret.setPlugin(plugin);
        return ret;
    }
    
    private static OAuthAccessToken authorize(List<NameValuePair> parameters) throws Exception {
        org.apache.http.client.ResponseHandler<String> responseHandler = new BasicResponseHandler();
        HttpPost httpPost = new HttpPost(MYOB_OAUTH2_TOKEN_URL);
        httpPost.setEntity(new UrlEncodedFormEntity(parameters));
        String responseBody = httpClient.execute(httpPost, responseHandler);
        log.debug("ResponseBody is {}", responseBody);
        OAuthAccessToken response = new Gson().fromJson(responseBody, OAuthAccessToken.class);        
        log.debug("access.token=" + response.getAccessToken());
        log.debug("refresh.token=" + response.getRefreshToken());
        return response;
    }
    

    private static final String encode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static final String decode(String s) {
        try {
            return URLDecoder.decode(s, "UTF-8");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

}


//{"access_token":"AAEAAKd9jCLIevqDo4Ej6wPz0W24DSgyPiRP5IzYZ66pVG2lsBJJXCV7vcDvmphidSKQARJep9z_QngABrALSu1hINITZAW5IsHQy_fAQbQX1HW7NWhQ7lUPRfd2cikt7JlC-UStringg5e3tsyYpF9cick_AcfkfvBbm1uiIiZRxHBxudMtYXbJzGSUPeHGU9HRYqv-parLCAwcQvvdPNWocAFHXmZiOxqHCbl4ToMVp3aAmpfyvQAGxbfD0bDQJ_gDVQFlNnzjbZGevABP4AwWGr1aS9uGD4kpQfWc15V7Jqgyt1KAaWIr41lSkRbVGgOC_V9DMijW7XRGBtGhuslRV-b5YmkAQAAAAEAAJTZEXj6XfS_VSRQ-cOltTZ4N87wPMOnsZsPl44ShfLLdMqmdxZkKhTvl-7XDPLTzCmxIhgyjODO_5MWBme-G8Bc7JDDTi7A9t7W9egIunEyyuBfKCBnnpdvWjuJYsUrvL5UiDk9SM_5uqKJvQFih1H20A7zX2aGA6dD9tya8-u5JqYxQWSfL0GiER62M9ZFAz6tQbkzvqk8P8Dm0y02jQPgZNt26__h1qWCrpI1EC4qXRpRejlmrygv2U-gXurl2CYW2imq-HIAPZA350QXO-rsqF2HG6oRMHjZfLrtnsqW85wqP9LngOnLFpN8xWSuYeg9BTlgVYNl2bi9OGaJHQGoAQ_kAx599onFdRbjDnFfhQqeWgbz6eYL7jX4iLBsltoGtm6rWVGtO2l4tiJec7EixYwbIlFhrShA7iLzzUO4yNUG_VICoD9ocbRstrx1PH3_tLrrOzz1dd4vWR_n524nvUvLcJaLyAXMiac6kONCH2FRchSNUcGlzq3yOHwxXFBWGfvR23DbhlaYNEVIuEiw6UFvzC05z_AzSXnsvKOo","token_type":"bearer","expires_in":"1200","refresh_token":"wOCm!IAAAAIn4b8nWsI8SoEFEbrvP3gpEr04USnpAlJ_KOE3uH0iFsQAAAAHeU_uWEL_PeIUjupXSHnMJ2Sz0Ao4ZMR5CUvl9zLUg5cWqzzXS9JhagSy-St6H6njxHFbVQzzQ08cI_FPzM90qFGA79gOP6TukoJIotIj2Aq7nzLOggbZt4iaSC28aydkaX1H6ZiMJIxv4tJk4eAHLh9sfkykap2C1Viqe4TFfr1K_8sHHhOfRVqLiw3kK-XyDGkhlFnIN9SXbCqOYkNbkwkGzygmemfxV1O_sh_edXw","scope":"CompanyFile","user":{"uid":"9178e187-6304-4d74-8e1d-8d431b36b251","username":"g@anahata.uno"}}
