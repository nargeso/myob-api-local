//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.payrollCategory;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import java.util.ArrayList;

/**
* Describes the Superannuation PayrollCategory
*/
public class PayrollCategorySuperannuation  extends PayrollCategory 
{
    /**
    * The account that is affected
    */
    private AccountLink ExpenseAccount;
    public AccountLink getExpenseAccount() {
        return ExpenseAccount;
    }

    public void setExpenseAccount(AccountLink value) {
        ExpenseAccount = value;
    }

    /**
    * The account that is affected
    */
    private AccountLink PayableAccount;
    public AccountLink getPayableAccount() {
        return PayableAccount;
    }

    public void setPayableAccount(AccountLink value) {
        PayableAccount = value;
    }

    /**
    * The type of superannuation contribution
    */
    private PayrollCategorySuperannuationContributionType ContributionType = PayrollCategorySuperannuationContributionType.SuperannuationGuarantee;
    public PayrollCategorySuperannuationContributionType getContributionType() {
        return ContributionType;
    }

    public void setContributionType(PayrollCategorySuperannuationContributionType value) {
        ContributionType = value;
    }

    /**
    * Print details on the pay advice
    */
    private boolean PrintOnPayAdvice;
    public boolean getPrintOnPayAdvice() {
        return PrintOnPayAdvice;
    }

    public void setPrintOnPayAdvice(boolean value) {
        PrintOnPayAdvice = value;
    }

    /**
    * Details about the calculation
    */
    private PayrollCategorySuperannuationCalculationBasis CalculationBasis;
    public PayrollCategorySuperannuationCalculationBasis getCalculationBasis() {
        return CalculationBasis;
    }

    public void setCalculationBasis(PayrollCategorySuperannuationCalculationBasis value) {
        CalculationBasis = value;
    }

    /**
    * The first amount of the wage to be excluded
    */
    private java.math.BigDecimal Exclusion;
    public java.math.BigDecimal getExclusion() {
        return Exclusion;
    }

    public void setExclusion(java.math.BigDecimal value) {
        Exclusion = value;
    }

    /**
    * Details about the limit
    */
    private PayrollCategorySuperannuationLimit Limit;
    public PayrollCategorySuperannuationLimit getLimit() {
        return Limit;
    }

    public void setLimit(PayrollCategorySuperannuationLimit value) {
        Limit = value;
    }

    /**
    * The threshold above which the PayrollCategory takes affect
    */
    private java.math.BigDecimal Threshold;
    public java.math.BigDecimal getThreshold() {
        return Threshold;
    }

    public void setThreshold(java.math.BigDecimal value) {
        Threshold = value;
    }

    /**
    * The 
    *  {@link #PayrollCategory}
    *  that are excluded dStringng calculations
    */
    private ArrayList<PayrollCategoryLink> Exemptions = new ArrayList<PayrollCategoryLink>();
    public ArrayList<PayrollCategoryLink> getExemptions() {
        return Exemptions;
    }

    public void setExemptions(ArrayList<PayrollCategoryLink> value) {
        Exemptions = value;
    }

}


