//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.contact;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.math.BigDecimal;

/**
* Bank Account
*/
public class BankAccount   
{
    /**
    * Account BSB number
    */
    private String BSBNumber;
    public String getBSBNumber() {
        return BSBNumber;
    }

    public void setBSBNumber(String value) {
        BSBNumber = value;
    }

    /**
    * Account number
    */
    private String BankAccountNumber;
    public String getBankAccountNumber() {
        return BankAccountNumber;
    }

    public void setBankAccountNumber(String value) {
        BankAccountNumber = value;
    }

    /**
    * Account name
    */
    private String BankAccountName;
    public String getBankAccountName() {
        return BankAccountName;
    }

    public void setBankAccountName(String value) {
        BankAccountName = value;
    }

    /**
    * Allocation amount, percent or dollar amount
    */
    private BigDecimal Value;
    public BigDecimal getValue() {
        return Value;
    }

    public void setValue(BigDecimal value) {
        Value = value;
    }

    /**
    * Allocation type
    */
    private AllocationType Unit = AllocationType.Percent;
    public AllocationType getUnit() {
        return Unit;
    }

    public void setUnit(AllocationType value) {
        Unit = value;
    }

}


