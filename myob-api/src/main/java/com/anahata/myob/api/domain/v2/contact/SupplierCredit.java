//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.contact;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */



/**
* Describes the supplier credit
*/
public class SupplierCredit   
{
    /**
    * The suppliers credit limit
    */
    private java.math.BigDecimal Limit;
    public java.math.BigDecimal getLimit() {
        return Limit;
    }

    public void setLimit(java.math.BigDecimal value) {
        Limit = value;
    }

    /**
    * The avilable credit limit
    */
    private java.math.BigDecimal Available;
    public java.math.BigDecimal getAvailable() {
        return Available;
    }

    public void setAvailable(java.math.BigDecimal value) {
        Available = value;
    }

    /**
    * The past due balance
    */
    private java.math.BigDecimal PastDue;
    public java.math.BigDecimal getPastDue() {
        return PastDue;
    }

    public void setPastDue(java.math.BigDecimal value) {
        PastDue = value;
    }

}


