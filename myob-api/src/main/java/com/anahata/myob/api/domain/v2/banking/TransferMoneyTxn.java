//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:20 PM
//

package com.anahata.myob.api.domain.v2.banking;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import com.anahata.myob.api.domain.v2.generalledger.CategoryLink;

/**
* A transfer money transaction entity
*/
public class TransferMoneyTxn  extends BaseEntity 
{
    /**
    * The account to pay from
    */
    private AccountLink FromAccount;
    public AccountLink getFromAccount() {
        return FromAccount;
    }

    public void setFromAccount(AccountLink value) {
        FromAccount = value;
    }

    /**
    * The account to pay to
    */
    private AccountLink ToAccount;
    public AccountLink getToAccount() {
        return ToAccount;
    }

    public void setToAccount(AccountLink value) {
        ToAccount = value;
    }

    /**
    * The transfer number
    */
    private String TransferNumber;
    public String getTransferNumber() {
        return TransferNumber;
    }

    public void setTransferNumber(String value) {
        TransferNumber = value;
    }

    /**
    * The date of the transfer
    */
    private java.util.Date Date = new java.util.Date();
    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date value) {
        Date = value;
    }

    /**
    * The amount transferred
    */
    private java.math.BigDecimal Amount;
    public java.math.BigDecimal getAmount() {
        return Amount;
    }

    public void setAmount(java.math.BigDecimal value) {
        Amount = value;
    }

    /**
    * A custom memo
    */
    private String Memo;
    public String getMemo() {
        return Memo;
    }

    public void setMemo(String value) {
        Memo = value;
    }

    /**
    * A category associated with the transfer
    */
    private CategoryLink Category;
    public CategoryLink getCategory() {
        return Category;
    }

    public void setCategory(CategoryLink value) {
        Category = value;
    }

}


