//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:22 PM
//

package com.anahata.myob.api.domain.v2.purchase;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.contact.PurchaseTerms;
import com.anahata.myob.api.domain.v2.contact.SupplierLink;
import com.anahata.myob.api.domain.v2.generalledger.CategoryLink;
import com.anahata.myob.api.domain.v2.generalledger.TaxCodeLink;
import com.anahata.myob.api.domain.v2.sale.OrderLayoutType;
import java.math.BigDecimal;

/**
* Describe the Purchase/Order resource
*/
public class PurchaseOrder  extends BaseEntity 
{
    /**
    * Purchase order number
    */
    private String Number;
    public String getNumber() {
        return Number;
    }

    public void setNumber(String value) {
        Number = value;
    }

    /**
    * Transaction date entry, format YYYY-MM-DD HH:MM:SS
    * { 'Date': '2013-08-11 13:33:02' }
    */
    private java.util.Date Date = new java.util.Date();
    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date value) {
        Date = value;
    }

    /**
    * Invoice number
    */
    private String SupplierInvoiceNumber;
    public String getSupplierInvoiceNumber() {
        return SupplierInvoiceNumber;
    }

    public void setSupplierInvoiceNumber(String value) {
        SupplierInvoiceNumber = value;
    }

    /**
    * The supplier
    */
    private SupplierLink Supplier;
    public SupplierLink getSupplier() {
        return Supplier;
    }

    public void setSupplier(SupplierLink value) {
        Supplier = value;
    }

    /**
    * Shipto Address
    */
    private String ShipToAddress;
    public String getShipToAddress() {
        return ShipToAddress;
    }

    public void setShipToAddress(String value) {
        ShipToAddress = value;
    }

    /**
    * Terms for the supplier
    */
    private PurchaseTerms Terms;
    public PurchaseTerms getTerms() {
        return Terms;
    }

    public void setTerms(PurchaseTerms value) {
        Terms = value;
    }

    /**
    * True indicates the transaction is set to tax inclusive.
    * False indicates the transaction is not tax inclusive.
    */
    private boolean IsTaxInclusive;
    public boolean getIsTaxInclusive() {
        return IsTaxInclusive;
    }

    public void setIsTaxInclusive(boolean value) {
        IsTaxInclusive = value;
    }

    /**
    * True indicates the transaction is taxable.False indicates the transaction is not taxable.
    */
    private boolean IsReportable;
    public boolean getIsReportable() {
        return IsReportable;
    }

    public void setIsReportable(boolean value) {
        IsReportable = value;
    }

    // Order = 9 is for Lines in PurchaseOrderWithLines class
    /**
    * Sum of all tax exclusive line amounts applicable to this purchase.
    */
    private java.math.BigDecimal Subtotal;
    public java.math.BigDecimal getSubtotal() {
        return Subtotal;
    }

    public void setSubtotal(java.math.BigDecimal value) {
        Subtotal = value;
    }

    /**
    * Tax freight amount applicable to the purchase order.
    * 
    * Only supported by Item or Service orders
    */
    private BigDecimal Freight;
    public BigDecimal getFreight() {
        return Freight;
    }

    public void setFreight(BigDecimal value) {
        Freight = value;
    }

    /**
    * The freight Tax code
    * 
    * Only supported by Item or Service orders
    */
    private TaxCodeLink FreightTaxCode;
    public TaxCodeLink getFreightTaxCode() {
        return FreightTaxCode;
    }

    public void setFreightTaxCode(TaxCodeLink value) {
        FreightTaxCode = value;
    }

    /**
    * Total of all tax amounts applicable to this purchase.
    */
    private java.math.BigDecimal TotalTax;
    public java.math.BigDecimal getTotalTax() {
        return TotalTax;
    }

    public void setTotalTax(java.math.BigDecimal value) {
        TotalTax = value;
    }

    /**
    * Total amount of the purchase order.
    */
    private java.math.BigDecimal TotalAmount;
    public java.math.BigDecimal getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(java.math.BigDecimal value) {
        TotalAmount = value;
    }

    /**
    * The category associated with the purchase
    */
    private CategoryLink Category;
    public CategoryLink getCategory() {
        return Category;
    }

    public void setCategory(CategoryLink value) {
        Category = value;
    }

    /**
    * Journal memo text describing the order.
    */
    private String JournalMemo;
    public String getJournalMemo() {
        return JournalMemo;
    }

    public void setJournalMemo(String value) {
        JournalMemo = value;
    }

    /**
    * How much has been paid to date
    */
    private java.math.BigDecimal AppliedToDate;
    public java.math.BigDecimal getAppliedToDate() {
        return AppliedToDate;
    }

    public void setAppliedToDate(java.math.BigDecimal value) {
        AppliedToDate = value;
    }

    /**
    * The outstanding balance
    */
    private java.math.BigDecimal BalanceDueAmount;
    public java.math.BigDecimal getBalanceDueAmount() {
        return BalanceDueAmount;
    }

    public void setBalanceDueAmount(java.math.BigDecimal value) {
        BalanceDueAmount = value;
    }

    /**
    * The current status of the purchase
    */
    private PurchaseOrderStatus Status = PurchaseOrderStatus.Open;
    public PurchaseOrderStatus getStatus() {
        return Status;
    }

    public void setStatus(PurchaseOrderStatus value) {
        Status = value;
    }

    /**
    * The date of the last payment made on the invoice
    */
    private java.util.Date LastPaymentDate = new java.util.Date();
    public java.util.Date getLastPaymentDate() {
        return LastPaymentDate;
    }

    public void setLastPaymentDate(java.util.Date value) {
        LastPaymentDate = value;
    }

    /**
    * The type of the order
    */
    private OrderLayoutType OrderType = OrderLayoutType.Service;
    public OrderLayoutType getOrderType() {
        return OrderType;
    }

    public void setOrderType(OrderLayoutType value) {
        OrderType = value;
    }

}


