//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:23 PM
//

package com.anahata.myob.api.domain.v2.sale;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.contact.CustomerLink;
import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import java.util.ArrayList;

/**
* Describe the CustomerPayment/RecordWithDiscountsAndFees resource
*/
public class CustomerPaymentRecordWithDiscountsAndFees   
{
    /**
    * The UID of the CustomerPayment (not yet supported)
    */
    private String UID;
    public String getUID() {
        return UID;
    }

    public void setUID(String value) {
        UID = value;
    }

    /**
    * 
    */
    private DepositTo DepositTo = com.anahata.myob.api.domain.v2.sale.DepositTo.Account;
    public DepositTo getDepositTo() {
        return DepositTo;
    }

    public void setDepositTo(DepositTo value) {
        DepositTo = value;
    }

    /**
    * The account to pay to (only need UID)
    */
    private AccountLink Account;
    public AccountLink getAccount() {
        return Account;
    }

    public void setAccount(AccountLink value) {
        Account = value;
    }

    /**
    * The customer making the payment
    */
    private CustomerLink Customer;
    public CustomerLink getCustomer() {
        return Customer;
    }

    public void setCustomer(CustomerLink value) {
        Customer = value;
    }

    /**
    * The receipt number to assign
    */
    private String ReceiptNumber;
    public String getReceiptNumber() {
        return ReceiptNumber;
    }

    public void setReceiptNumber(String value) {
        ReceiptNumber = value;
    }

    /**
    * The date the transaction was made
    */
    private java.util.Date Date = new java.util.Date();
    public java.util.Date getDate() {
        return Date;
    }

    public void setDate(java.util.Date value) {
        Date = value;
    }

    /**
    * The finance charge to apply
    */
    private java.math.BigDecimal FinanceCharge;
    public java.math.BigDecimal getFinanceCharge() {
        return FinanceCharge;
    }

    public void setFinanceCharge(java.math.BigDecimal value) {
        FinanceCharge = value;
    }

    /**
    * The payment method
    */
    private String PaymentMethod;
    public String getPaymentMethod() {
        return PaymentMethod;
    }

    public void setPaymentMethod(String value) {
        PaymentMethod = value;
    }

    /**
    * Supplementary data
    */
    private String Memo;
    public String getMemo() {
        return Memo;
    }

    public void setMemo(String value) {
        Memo = value;
    }

    /**
    * The Invoices or Orders being affected
    */
    private ArrayList<CustomerPaymentRecordWithDiscountsAndFeesLine> Lines = new ArrayList<CustomerPaymentRecordWithDiscountsAndFeesLine>();
    public ArrayList<CustomerPaymentRecordWithDiscountsAndFeesLine> getLines() {
        return Lines;
    }

    public void setLines(ArrayList<CustomerPaymentRecordWithDiscountsAndFeesLine> value) {
        Lines = value;
    }

}


