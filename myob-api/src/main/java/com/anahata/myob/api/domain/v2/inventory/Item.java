//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.inventory;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.generalledger.AccountLink;
import com.anahata.myob.api.domain.v2.Identifier;
import java.math.BigDecimal;

/**
* Item
*/
public class Item  extends BaseEntity 
{
    /**
    * Initialise an Item entity
    */
    public Item() {
        setIsActive(true);
    }

    /**
    * Item number.
    */
    private String Number;
    public String getNumber() {
        return Number;
    }

    public void setNumber(String value) {
        Number = value;
    }

    /**
    * Name of the item.
    */
    private String Name;
    public String getName() {
        return Name;
    }

    public void setName(String value) {
        Name = value;
    }

    /**
    * True indicates the item is active. (default)False indicates the item is inactive.
    */
    private boolean IsActive;
    public boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(boolean value) {
        IsActive = value;
    }

    /**
    * A description of the item.
    */
    private String Description;
    public String getDescription() {
        return Description;
    }

    public void setDescription(String value) {
        Description = value;
    }

    /**
    * True indicates to use the description text instead of item name on sale invoices and purchase orders.False indicates not to use the item description on sales and purchases.
    */
    private boolean UseDescription;
    public boolean getUseDescription() {
        return UseDescription;
    }

    public void setUseDescription(boolean value) {
        UseDescription = value;
    }

    /**
    * Item custom list1
    */
    private Identifier CustomList1;
    public Identifier getCustomList1() {
        return CustomList1;
    }

    public void setCustomList1(Identifier value) {
        CustomList1 = value;
    }

    /**
    * Item custom list2
    */
    private Identifier CustomList2;
    public Identifier getCustomList2() {
        return CustomList2;
    }

    public void setCustomList2(Identifier value) {
        CustomList2 = value;
    }

    /**
    * Item custom list3
    */
    private Identifier CustomList3;
    public Identifier getCustomList3() {
        return CustomList3;
    }

    public void setCustomList3(Identifier value) {
        CustomList3 = value;
    }

    /**
    * Item custom field1
    */
    private Identifier CustomField1;
    public Identifier getCustomField1() {
        return CustomField1;
    }

    public void setCustomField1(Identifier value) {
        CustomField1 = value;
    }

    /**
    * Item custom field2
    */
    private Identifier CustomField2;
    public Identifier getCustomField2() {
        return CustomField2;
    }

    public void setCustomField2(Identifier value) {
        CustomField2 = value;
    }

    /**
    * Item custom field3
    */
    private Identifier CustomField3;
    public Identifier getCustomField3() {
        return CustomField3;
    }

    public void setCustomField3(Identifier value) {
        CustomField3 = value;
    }

    /**
    * Quantity of units held in inventory
    */
    private java.math.BigDecimal QuantityOnHand;
    public java.math.BigDecimal getQuantityOnHand() {
        return QuantityOnHand;
    }

    public void setQuantityOnHand(java.math.BigDecimal value) {
        QuantityOnHand = value;
    }

    /**
    * Quantity of the item held in pending sale invoices.
    */
    private java.math.BigDecimal QuantityCommitted;
    public java.math.BigDecimal getQuantityCommitted() {
        return QuantityCommitted;
    }

    public void setQuantityCommitted(java.math.BigDecimal value) {
        QuantityCommitted = value;
    }

    /**
    * Quantity of the item held in pending purchase orders.
    */
    private java.math.BigDecimal QuantityOnOrder;
    public java.math.BigDecimal getQuantityOnOrder() {
        return QuantityOnOrder;
    }

    public void setQuantityOnOrder(java.math.BigDecimal value) {
        QuantityOnOrder = value;
    }

    /**
    * Calculated quantity of the item available for sale.
    */
    private java.math.BigDecimal QuantityAvailable;
    public java.math.BigDecimal getQuantityAvailable() {
        return QuantityAvailable;
    }

    public void setQuantityAvailable(java.math.BigDecimal value) {
        QuantityAvailable = value;
    }

    /**
    * Item's average cost when the quantity on hand is equal to or greater than zero.
    */
    private BigDecimal AverageCost;
    public BigDecimal getAverageCost() {
        return AverageCost;
    }

    public void setAverageCost(BigDecimal value) {
        AverageCost = value;
    }

    /**
    * Dollar value of units held in inventory.
    */
    private java.math.BigDecimal CurrentValue;
    public java.math.BigDecimal getCurrentValue() {
        return CurrentValue;
    }

    public void setCurrentValue(java.math.BigDecimal value) {
        CurrentValue = value;
    }

    /**
    * Item's base selling price.
    */
    private java.math.BigDecimal BaseSellingPrice;
    public java.math.BigDecimal getBaseSellingPrice() {
        return BaseSellingPrice;
    }

    public void setBaseSellingPrice(java.math.BigDecimal value) {
        BaseSellingPrice = value;
    }

    /**
    * The item is bought
    */
    private boolean IsBought;
    public boolean getIsBought() {
        return IsBought;
    }

    public void setIsBought(boolean value) {
        IsBought = value;
    }

    /**
    * The item is sold
    */
    private boolean IsSold;
    public boolean getIsSold() {
        return IsSold;
    }

    public void setIsSold(boolean value) {
        IsSold = value;
    }

    /**
    * The item is inventoried
    */
    private boolean IsInventoried;
    public boolean getIsInventoried() {
        return IsInventoried;
    }

    public void setIsInventoried(boolean value) {
        IsInventoried = value;
    }

    /**
    * An account that is used when 
    *  {@link #IsBought}
    *  is true and 
    *  {@link #IsInventoried}
    *  is false
    */
    private AccountLink ExpenseAccount;
    public AccountLink getExpenseAccount() {
        return ExpenseAccount;
    }

    public void setExpenseAccount(AccountLink value) {
        ExpenseAccount = value;
    }

    /**
    * An account that is used when 
    *  {@link #IsSold}
    *  is true and 
    *  {@link #IsInventoried}
    *  is true
    */
    private AccountLink CostOfSalesAccount;
    public AccountLink getCostOfSalesAccount() {
        return CostOfSalesAccount;
    }

    public void setCostOfSalesAccount(AccountLink value) {
        CostOfSalesAccount = value;
    }

    /**
    * An account that is used when 
    *  {@link #IsSold}
    *  is true
    */
    private AccountLink IncomeAccount;
    public AccountLink getIncomeAccount() {
        return IncomeAccount;
    }

    public void setIncomeAccount(AccountLink value) {
        IncomeAccount = value;
    }

    /**
    * An account that is used when 
    *  {@link #IsInventoried}
    *  is true
    */
    private AccountLink AssetAccount;
    public AccountLink getAssetAccount() {
        return AssetAccount;
    }

    public void setAssetAccount(AccountLink value) {
        AssetAccount = value;
    }

    /**
    * The details when 
    *  {@link #IsBought}
    *  is true
    */
    private ItemBuyingDetails BuyingDetails;
    public ItemBuyingDetails getBuyingDetails() {
        return BuyingDetails;
    }

    public void setBuyingDetails(ItemBuyingDetails value) {
        BuyingDetails = value;
    }

    /**
    * The detaisl when 
    *  {@link #IsSold}
    *  is true
    */
    private ItemSellingDetails SellingDetails;
    public ItemSellingDetails getSellingDetails() {
        return SellingDetails;
    }

    public void setSellingDetails(ItemSellingDetails value) {
        SellingDetails = value;
    }

    /**
    * Uniform resource identifier to retrieve the Item's photo
    */
    private String PhotoURI;
    public String getPhotoURI() {
        return PhotoURI;
    }

    public void setPhotoURI(String value) {
        PhotoURI = value;
    }

}


