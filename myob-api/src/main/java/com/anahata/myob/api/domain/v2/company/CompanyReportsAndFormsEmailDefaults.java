//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:20 PM
//

package com.anahata.myob.api.domain.v2.company;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.company.EmailDefaultsPurchases;
import com.anahata.myob.api.domain.v2.company.EmailDefaultsSales;
import com.anahata.myob.api.domain.v2.company.EmailDefaultsTemplateBase;

/**
* Email default templates preferences
*/
public class CompanyReportsAndFormsEmailDefaults   
{
    /**
    * Email defaults for sales
    */
    private EmailDefaultsSales Sales;
    public EmailDefaultsSales getSales() {
        return Sales;
    }

    public void setSales(EmailDefaultsSales value) {
        Sales = value;
    }

    /**
    * Email defaults for purchases
    */
    private EmailDefaultsPurchases Purchases;
    public EmailDefaultsPurchases getPurchases() {
        return Purchases;
    }

    public void setPurchases(EmailDefaultsPurchases value) {
        Purchases = value;
    }

    /**
    * Email defaults for pay slip
    */
    private EmailDefaultsTemplateBase PaySlips;
    public EmailDefaultsTemplateBase getPaySlips() {
        return PaySlips;
    }

    public void setPaySlips(EmailDefaultsTemplateBase value) {
        PaySlips = value;
    }

}


