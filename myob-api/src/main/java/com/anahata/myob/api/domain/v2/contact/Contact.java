//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.contact;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import com.anahata.myob.api.domain.v2.Identifier;
import java.util.ArrayList;

/**
* A contact entity
*/
public class Contact  extends BaseEntity 
{
    /**
    * Initialises a new instance of the Contact class
    */
    public Contact() {
        setIsActive(true);
    }

    /**
    * The company name
    */
    private String CompanyName;
    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String value) {
        CompanyName = value;
    }

    /**
    * Last Name / Company Name
    */
    private String LastName;
    public String getLastName() {
        return LastName;
    }

    public void setLastName(String value) {
        LastName = value;
    }

    /**
    * First Name
    */
    private String FirstName;
    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String value) {
        FirstName = value;
    }

    /**
    * Is the card an individual or company
    */
    private boolean IsIndividual;
    public boolean getIsIndividual() {
        return IsIndividual;
    }

    public void setIsIndividual(boolean value) {
        IsIndividual = value;
    }

    /**
    * Card Identifier
    */
    private String DisplayID;
    public String getDisplayID() {
        return DisplayID;
    }

    public void setDisplayID(String value) {
        DisplayID = value;
    }

    /**
    * Status of the Customer
    */
    private boolean IsActive;
    public boolean getIsActive() {
        return IsActive;
    }

    public void setIsActive(boolean value) {
        IsActive = value;
    }

    /**
    * Notes
    */
    private String Notes;
    public String getNotes() {
        return Notes;
    }

    public void setNotes(String value) {
        Notes = value;
    }

    /**
    * Identifiers
    */
    private String Tags;
    public String getTags() {
        return Tags;
    }

    public void setTags(String value) {
        Tags = value;
    }

    /**
    * Customer Balance
    */
    private java.math.BigDecimal CurrentBalance;
    public java.math.BigDecimal getCurrentBalance() {
        return CurrentBalance;
    }

    public void setCurrentBalance(java.math.BigDecimal value) {
        CurrentBalance = value;
    }

    /**
    * A number of adresses (max 5)
    */
    private ArrayList<Address> Addresses = new ArrayList<Address>();
    public ArrayList<Address> getAddresses() {
        return Addresses;
    }

    public void setAddresses(ArrayList<Address> value) {
        Addresses = value;
    }

    /**
    * The identifiers applied to this contact
    */
    private ArrayList<Identifier> Identifiers = new ArrayList<Identifier>();
    public ArrayList<Identifier> getIdentifiers() {
        return Identifiers;
    }

    public void setIdentifiers(ArrayList<Identifier> value) {
        Identifiers = value;
    }

    /**
    * An identifier
    */
    private Identifier CustomList1;
    public Identifier getCustomList1() {
        return CustomList1;
    }

    public void setCustomList1(Identifier value) {
        CustomList1 = value;
    }

    /**
    * An identifier
    */
    private Identifier CustomList2;
    public Identifier getCustomList2() {
        return CustomList2;
    }

    public void setCustomList2(Identifier value) {
        CustomList2 = value;
    }

    /**
    * An identifier
    */
    private Identifier CustomList3;
    public Identifier getCustomList3() {
        return CustomList3;
    }

    public void setCustomList3(Identifier value) {
        CustomList3 = value;
    }

    /**
    * An identifier
    */
    private Identifier CustomField1;
    public Identifier getCustomField1() {
        return CustomField1;
    }

    public void setCustomField1(Identifier value) {
        CustomField1 = value;
    }

    /**
    * An identifier
    */
    private Identifier CustomField2;
    public Identifier getCustomField2() {
        return CustomField2;
    }

    public void setCustomField2(Identifier value) {
        CustomField2 = value;
    }

    /**
    * An identifier
    */
    private Identifier CustomField3;
    public Identifier getCustomField3() {
        return CustomField3;
    }

    public void setCustomField3(Identifier value) {
        CustomField3 = value;
    }

    /**
    * Read only - only returned for 
    *  {@link #Contact}
    *  entities and not its subtypes.
    */
    private ContactType Type = ContactType.Customer;
    public ContactType getType() {
        return Type;
    }

    public void setType(ContactType value) {
        Type = value;
    }

    /**
    * The String to a photo (image) of this contact
    */
    private String PhotoURI;
    public String getPhotoURI() {
        return PhotoURI;
    }

    public void setPhotoURI(String value) {
        PhotoURI = value;
    }

}


