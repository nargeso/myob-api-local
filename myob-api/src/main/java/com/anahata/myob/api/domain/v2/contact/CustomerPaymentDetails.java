//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.contact;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */



/**
* Describes the customer payment details
*/
public class CustomerPaymentDetails   
{
    /**
    * Default payment method
    */
    private String Method;
    public String getMethod() {
        return Method;
    }

    public void setMethod(String value) {
        Method = value;
    }

    /**
    * The card number (last 4 digits only)
    */
    private String CardNumber;
    public String getCardNumber() {
        return CardNumber;
    }

    public void setCardNumber(String value) {
        CardNumber = value;
    }

    /**
    * Default name on the card
    */
    private String NameOnCard;
    public String getNameOnCard() {
        return NameOnCard;
    }

    public void setNameOnCard(String value) {
        NameOnCard = value;
    }

    /**
    * The bank account BSB number
    */
    private String BSBNumber;
    public String getBSBNumber() {
        return BSBNumber;
    }

    public void setBSBNumber(String value) {
        BSBNumber = value;
    }

    /**
    * The bank account number
    */
    private String BankAccountNumber;
    public String getBankAccountNumber() {
        return BankAccountNumber;
    }

    public void setBankAccountNumber(String value) {
        BankAccountNumber = value;
    }

    /**
    * The bank account name
    */
    private String BankAccountName;
    public String getBankAccountName() {
        return BankAccountName;
    }

    public void setBankAccountName(String value) {
        BankAccountName = value;
    }

    /**
    * Additional notes
    */
    private String Notes;
    public String getNotes() {
        return Notes;
    }

    public void setNotes(String value) {
        Notes = value;
    }

}


