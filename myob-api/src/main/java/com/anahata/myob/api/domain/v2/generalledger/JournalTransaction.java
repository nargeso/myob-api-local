//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:21 PM
//

package com.anahata.myob.api.domain.v2.generalledger;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.BaseEntity;
import java.util.ArrayList;

/**
* Describes a Journal Transaction  resource
*/
public class JournalTransaction  extends BaseEntity 
{
    /**
    * Journal transaction id.
    */
    private String DisplayID;
    public String getDisplayID() {
        return DisplayID;
    }

    public void setDisplayID(String value) {
        DisplayID = value;
    }

    /**
    * The entry type
    */
    private JournalType journalType = JournalType.General;
    public JournalType getJournalType() {
        return journalType;
    }

    public void setJournalType(JournalType value) {
        journalType = value;
    }

    /**
    * Transaction date entry
    */
    private java.util.Date DateOccurred = new java.util.Date();
    public java.util.Date getDateOccurred() {
        return DateOccurred;
    }

    public void setDateOccurred(java.util.Date value) {
        DateOccurred = value;
    }

    /**
    * Date of the transaction
    */
    private java.util.Date DatePosted = new java.util.Date();
    public java.util.Date getDatePosted() {
        return DatePosted;
    }

    public void setDatePosted(java.util.Date value) {
        DatePosted = value;
    }

    /**
    * Journal memo assigned to the transaction.
    */
    private String Description;
    public String getDescription() {
        return Description;
    }

    public void setDescription(String value) {
        Description = value;
    }

    /**
    * Journal line information
    */
    private ArrayList<JournalTransactionLine> Lines = new ArrayList<JournalTransactionLine>();
    public ArrayList<JournalTransactionLine> getLines() {
        return Lines;
    }

    public void setLines(ArrayList<JournalTransactionLine> value) {
        Lines = value;
    }

    /**
    * Source transaction information
    */
    private SourceTransaction SourceTransaction;
    public SourceTransaction getSourceTransaction() {
        return SourceTransaction;
    }

    public void setSourceTransaction(SourceTransaction value) {
        SourceTransaction = value;
    }

}


