/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anahata.myob.api.service;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.anahata.myob.api.MyobEndPoint;
import com.anahata.myob.api.MyobEndPointProvider;
import com.anahata.myob.api.auth.*;
import com.anahata.myob.api.domain.CompanyFile;

/**
 *
 * @author pablo
 */
public class CompanyFileService extends AbstractMyobService{
    
    public CompanyFileService() {
        super(null);
    }
    
    public CompanyFile[] findAll() {
        String ret = sendReceive(null, null, null);        
        return fromJson(ret, new CompanyFile[0].getClass());
    }
    
    public static CompanyFile[] findAll(OAuthAccessToken token) throws Exception {
        CompanyFileService cfs = new CompanyFileService();
        cfs.setEndPointProvider(new MyobEndPointProvider() {
            @Override
            public MyobEndPoint getEndPoint() {
                return new MyobEndPoint(MyobEndPoint.CLOUD_ENDPOINT, new DataFileCredentials(), token);
            }

            @Override
            public void oauthTokenRefreshed(MyobEndPoint locator) {
                
            }
        });
        return cfs.findAll();
    }
}
