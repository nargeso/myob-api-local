//
// Translated by CS2J (http://www.cs2j.com): 8/12/2014 6:01:20 PM
//

package com.anahata.myob.api.domain.v2.company;

/*
 * #%L
 * myob-api
 * %%
 * Copyright (C) 2012 - 2014 <A HREF="http://www.anahata.uno">Anahata Technologies Pty Ltd</A>
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.anahata.myob.api.domain.v2.company.EmailDefaultsTemplateBase;

/**
* Email defaults template inlcude subject, message and flag of including transaction number in subject
*/
public class EmailDefaultsTemplate  extends EmailDefaultsTemplateBase 
{
    /**
    * Indicates whether or not include transaction number in subject
    */
    private boolean IncludeNumberInSubject;
    public boolean getIncludeNumberInSubject() {
        return IncludeNumberInSubject;
    }

    public void setIncludeNumberInSubject(boolean value) {
        IncludeNumberInSubject = value;
    }

}


